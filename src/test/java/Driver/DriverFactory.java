package Driver;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;


public class DriverFactory {
    protected static AppiumDriver driver;

    /*public DriverFactory() throws MalformedURLException {
        initializeAppiumSession();
    }
    protected void initializeAppiumSession() throws MalformedURLException {
     configureSessionForDevice();
    //  configureSessionForEmulator();
    }*/

@BeforeClass
    // Android device details
    public void configureSessionForDevice() throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("platformVersion", Device.platformVersion);
        capabilities.setCapability("deviceName", Device.deviceName);
        capabilities.setCapability("platformName", Device.platformName);
        capabilities.setCapability("udid", Device.udid);
        capabilities.setCapability("app", "/Users/artem/Documents/app-debug.apk");
        capabilities.setCapability("appPackage", "com.asperasoft.android.asperaoncloud.debug");
        capabilities.setCapability("appActivity", "com.asperasoft.android.files.presentation.ui.activity.SplashScreenActivity");
        capabilities.setCapability("automationName", "UiAutomator2");

    driver =  new AndroidDriver(new URL(Device.URL), capabilities);
    }

@AfterClass
    public void killAppiumSession(){
        driver.quit();
    }
}