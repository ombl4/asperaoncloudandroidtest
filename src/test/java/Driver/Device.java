package Driver;

public class Device {
    public static final String platformName = "Android";
    public static final String deviceName = "Pixel API 27";
    public static final String platformVersion = "8.1";
    public static final String URL = "http://127.0.0.1:4723/wd/hub";
    public static final String udid = "emulator-5554";
    public static final String appName = "app-debug.apk";
    public static final String appDir = "/Users/artem/Documents/IntelliJ/asperaoncloudandroidtest/src/test/resources/";
    public static final String appPackage = "com.asperasoft.android.asperaoncloud.debug";
    public static final String appActivity = "com.asperasoft.android.files.presentation.ui.activity.SplashScreenActivity";
}
