package AppPages;


import Driver.DriverFactory;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.By;
import io.appium.java_client.pagefactory.AndroidBy;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class WelcomeScreen extends AbstractScreen {

 @AndroidFindBy(id = "button1")
 public AndroidElement loginToYourAccountButton;

    public WelcomeScreen (AppiumDriver driver) { super (driver);}

    public void clickLoginButtonMethod () {
        loginToYourAccountButton.click();
    }
}
